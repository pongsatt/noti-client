package client

import (
	cloudevents "github.com/cloudevents/sdk-go"
)

// Notification client
type Notification struct {
	uri string
}

// New creates notification client
func New() *Notification {
	uri := getURI()

	return &Notification{uri}
}

// Send sends notification event
func (noti *Notification) Send(event cloudevents.Event) error {
	return send(noti.uri, event)
}
