module gitlab.com/pongsatt/noti-client

go 1.12

require (
	github.com/cloudevents/sdk-go v0.0.0-20190708160832-90ac00aaebde
	github.com/gogo/protobuf v1.2.0 // indirect
	github.com/google/uuid v1.1.1
)
