package client

import (
	cloudevents "github.com/cloudevents/sdk-go"
	"github.com/google/uuid"
)

// FCMNotification provides FCM notification structure
type FCMNotification struct {
	Title       string `json:"title"`
	Body        string `json:"body"`
	Icon        string `json:"icon"`
	ClickAction string `json:"click_action"`
}

// FCMMessage provides FMC Message structure
type FCMMessage struct {
	Notification FCMNotification `json:"notification"`
	To           string          `json:"to"`
}

func buildFCMNotificationEvent(source string, msg FCMMessage) cloudevents.Event {
	event := cloudevents.NewEvent()
	event.SetID(uuid.New().String())
	event.SetType("fcm")
	event.SetSource(source)
	event.SetData(msg)
	return event
}

// BuildFCMMessage build FCM message from argument
func BuildFCMMessage(to, title, body, icon, link string) FCMMessage {
	return FCMMessage{
		To: to,
		Notification: FCMNotification{
			Title:       title,
			Body:        body,
			Icon:        icon,
			ClickAction: link,
		},
	}
}

// SendFCM sends fcm notification
func (noti *Notification) SendFCM(source string, msg FCMMessage) (*cloudevents.Event, error) {
	event := buildFCMNotificationEvent(source, msg)
	err := noti.Send(event)

	return &event, err
}
