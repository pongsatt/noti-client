package client

import (
	"os"
)

func getURI() string {
	customHost := os.Getenv("EVENT_BROKER_URL")

	if customHost != "" {
		return customHost
	}

	return "http://default-broker.default.svc.cluster.local/"
}
