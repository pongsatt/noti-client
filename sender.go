package client

import (
	"context"
	"time"

	cloudevents "github.com/cloudevents/sdk-go"
)

func send(uri string, event cloudevents.Event) error {
	t, err := cloudevents.NewHTTPTransport(
		cloudevents.WithTarget(uri),
		cloudevents.WithEncoding(cloudevents.HTTPBinaryV02),
	)

	if err != nil {
		return err
	}

	c, err := cloudevents.NewClient(t)
	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
	defer cancel()

	if _, err := c.Send(ctx, event); err != nil {
		return err
	}

	return nil
}
