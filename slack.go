package client

import (
	cloudevents "github.com/cloudevents/sdk-go"
	"github.com/google/uuid"
)

type slackMessage struct {
	Channel string
	Text    string
}

func buildSlackNotificationEvent(source, channel, msg string) cloudevents.Event {
	event := cloudevents.NewEvent()
	event.SetID(uuid.New().String())
	event.SetType("slack")
	event.SetSource(source)
	event.SetData(slackMessage{channel, msg})
	return event
}

// SendSlack sends slack notification
func (noti *Notification) SendSlack(source, channel, msg string) (*cloudevents.Event, error) {
	event := buildSlackNotificationEvent(source, channel, msg)
	err := noti.Send(event)

	return &event, err
}
